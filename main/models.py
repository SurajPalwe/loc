from django.db import models

# Create your models here.

class Blog(models.Model):
	title = models.CharField(max_length=300)
	meta_tags = models.CharField(max_length=300)
	header_image_url = models.CharField(max_length=300)
	body = models.TextField()
	slug = models.CharField(max_length=300)
	username = models.CharField(max_length=300)
	is_active = models.BooleanField(default=True)
	is_deleted = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.title

class NewsLetterSubscription(models.Model):
	email = models.CharField(max_length=300)
	is_active = models.BooleanField(default=True)
	is_deleted = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.email