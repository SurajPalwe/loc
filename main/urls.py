from django.urls import path

from . import views, admin_views

handler404 = 'main.views.handler404'

urlpatterns = [
    path('', views.index, name='index'),
    path('about_us', views.about_us, name='about_us'),
    path('franchise', views.franchise, name='franchise'),
	path('products', views.products, name='products'),
	path('blog_page', views.blog_page, name='blog_page'), 
    path('contact_us', views.contact_us, name='contact_us'),
    path('media', views.media, name='media'),
    path('reach_out_form', views.reach_out_form, name='reach_out_form'),
    path(r'blog/<slug:slug>', views.single_blog, name='single_blog'),
    path(r'store_locator', views.store_locator, name='store_locator'),
    path(r'team', views.team, name='team'),
    path(r'career', views.career, name='career'),
    path(r'products', views.products, name='products'),
    path(r'newsletter_subscription', views.newsletter_subscription, name='newsletter_subscription'),
    path(r'googleee3195f7154d85e9', views.googleee3195f7154d85e9, name='googleee3195f7154d85e9'),

    path('create_blog', admin_views.create_blog, name='create_blog'),
    path('admin_login', admin_views.admin_login, name='admin_login'),
    path('admin_home', admin_views.admin_home, name='admin_home'),
    path('admin_logout', admin_views.admin_logout, name='admin_logout'),
    path('newsletter_data', admin_views.newsletter_data, name='newsletter_data'),
]