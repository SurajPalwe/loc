from django.http import HttpResponse
from django.shortcuts import render,redirect
from django.views.decorators.csrf import csrf_exempt
from main.utility import *
from main.models import Blog, NewsLetterSubscription
import pdb;

def index(request):
    if request.user_agent.is_mobile:
        return render(request, 'mobile/mobile_main.html')
    else:
        return render(request, 'main.html')

def about_us(request):
    if request.user_agent.is_mobile:
        return render(request, 'mobile/about_us_mobile.html')
    else:
        return render(request, 'about_us.html')

def franchise(request):
    if request.method == 'POST':
        send_franchise_contacted_email(request)
    if request.user_agent.is_mobile:
        return render(request, 'mobile/franchise_mobile.html')
    else:
        return render(request, 'franchise.html')

def products(request):
    return render(request, 'products.html')

def blog_page(request):
    blogs = Blog.objects.filter(is_active=True).order_by('-created_at')
    data = {
        'blogs' : [blogs[r*3:(r+1)*3] for r in range(0,3)],
    }
    if request.user_agent.is_mobile:
        return render(request, 'mobile/blog_mobile.html',data)
    else:
        return render(request, 'blog.html', data)

def contact_us(request):
    if request.method == 'POST':
        send_contact_us_email(request)
    if request.user_agent.is_mobile:
        return render(request, 'mobile/contact_us_mobile.html')
    else:
        return render(request, 'contact_us.html')

@csrf_exempt
def reach_out_form(request):
    '''
        This handler will capture the details from form and send it as email
    '''
    message = ""
    if request.is_ajax():
        name = request.POST.get('name','')
        email = request.POST.get('email','')
        phone_number = request.POST.get('phone_number','')
        msg = request.POST.get('msg','')
        if name and email and phone_number and msg:
            message = send_reach_out_email(name, email, phone_number, msg)
    else:
        message = "Failed"
    return HttpResponse(message)

def handler404(request, exception):
    context = {}
    response = render(request, "404.html", context=context)
    response.status_code = 404
    return response

def single_blog(request, slug=None):
    try:
        blog = Blog.objects.get(slug=slug, is_active=True)
        nextblogs = get_next_blogs(blog.id)
    except Blog.DoesNotExist:
        return render(request, "404.html", context={})
    data = {
        'blog' : blog,
        'nextblogs' : nextblogs
    }
    return render(request, 'single_blog.html',data)

def store_locator(request):
    if request.user_agent.is_mobile:
        return render(request, 'mobile/store_locator_mobile.html')
    else:
        return render(request, "store_locator.html",{})

def team(request):
    if request.user_agent.is_mobile:
        return render(request, 'mobile/team_mobile.html')
    else:
        return render(request, "team.html",{})

def career(request):
    if request.user_agent.is_mobile:
        return render(request, 'mobile/career_mobile.html')
    else:
        return render(request, "career.html",{})

def products(request):
    data = {
        # 'alert_message' : "Product Listing successfull",
        # 'msg_type' : "danger"
    }
    if request.user_agent.is_mobile:
        return render(request, 'mobile/products_mobile.html')
    else:
        return render(request, "products.html",data)

@csrf_exempt
def newsletter_subscription(request):
    if request.is_ajax():
        email = request.POST.get('email','')
        obj, is_created = NewsLetterSubscription.objects.get_or_create(email=email)
        return HttpResponse(is_created)
    else:
        return HttpResponse("Error")

def media(request):
    if request.user_agent.is_mobile:
        return render(request, 'mobile/mobile_social_media.html')
    else:
        return render(request, "social_media.html",{})

def googleee3195f7154d85e9(request):
    #This view is for google search console verfication
    return render(request , 'googleee3195f7154d85e9.html')