import tweepy
import facebook
import requests
import json

follower_count = {}

def init_twitter():
    twitter_auth_keys = {
        "consumer_key": "NN57I81XJnEQINEqxo8dxJ9va",
        "consumer_secret": "sEmM6P6SeRBn8dMPAq0mN9pG94paq5SjbtxldtCd3eoaPD7tLX",
        "access_token": "1310564436336230400-LDWfUVsjHbzFPCjdDkAnd34JL0rX2K",
        "access_token_secret": "gvKpaQd4oE9SYDcQrMFLcH9GZ4g73pQhrfnlTKRF5XAH1",
    }
    auth = tweepy.OAuthHandler(
        twitter_auth_keys["consumer_key"], twitter_auth_keys["consumer_secret"]
    )
    auth.set_access_token(
        twitter_auth_keys["access_token"], twitter_auth_keys["access_token_secret"]
    )
    api = tweepy.API(auth)
    return api

def twitter_post():
    api = init_twitter()
    tweet = "Tweet Should start from here. :P "
    status = api.update_status(status=tweet)

def get_twitter_followers_count():
    username = 'surajpalwe'
    followers = init_twitter().get_user(username).followers_count
    follower_count['twitter_follower_count'] = followers

def get_facebook_page_count():
    api = get_fb_api()
    page = graph.get_object("108421947731044", fields='id,name,fan_count')

def fb_init_token():
    app_id = '2741789596104249'
    app_secret = 'd067e22311a73c04077e655ceee60cb5'
    graph = facebook.GraphAPI()
    return graph.get_app_access_token(app_id, app_secret)


def facebook_post():
    api = get_fb_api()
    msg = "Hello, world!"
    status = api.put_object("me", "feed", message=msg)

def get_fb_api():
    cfg = {
        "page_id": "108421947731044",  # Step 1
        "access_token": "EAAm9pKLstjkBAIm3xpqyci4m7ZBc4w5KLFY9NZCpHhnpCVhiPH4Gjc8qNcFvZBOPMFomWsgGC8KGNdLmXLPyjL0I0cESr4hblSKdN8yjIy1d83XerzJn6WiLbMSZAZClWrjO1rFTWPGcJv8HAdBWZCwtHt5kQXVtSlXY7P3LGJZCkWTAXPXZAZAwpngRc2nlmvQ5hD1ZAZCeGvS0Mfc9zJHHUmkZCXnnRxTHqYIZD",  # Step 3
    }
    graph = facebook.GraphAPI(fb_init_token())
    # Get page token to post as the page. You can skip
    # the following if you want to post as yourself.
    resp = graph.get_object("me/accounts")
    page_access_token = None
    for page in resp["data"]:
        if page["id"] == cfg["page_id"]:
            page_access_token = page["access_token"]
    graph = facebook.GraphAPI(page_access_token)
    return graph

def get_instagram_followers_count():
    '''
        return followers count
    '''
    username = "loc_lifeofchai"
    url = 'https://www.instagram.com/' + username
    r = requests.get(url).text

    start = '"edge_followed_by":{"count":'
    end = '},"followed_by_viewer"'
    followers= r[r.find(start)+len(start):r.rfind(end)]

    start = '"edge_follow":{"count":'
    end = '},"follows_viewer"'
    following= r[r.find(start)+len(start):r.rfind(end)]
    follower_count['instagram_followers'] = followers
    follower_count['instagram_following'] = following

def write_followers_to_file():
    with open('social_media_count.json', 'w') as outfile:
        json.dump(follower_count, outfile)

if __name__ == "__main__":
    get_instagram_followers_count()
    get_twitter_followers_count()
    write_followers_to_file()


