'''
	This file will be used to write utilities required
'''
from django.template.loader import render_to_string
from django.core.mail import send_mail, EmailMultiAlternatives
from django.conf import settings
from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from main.models import Blog
from django.utils.text import slugify
from django.apps import apps
import json
import pdb;


def send_reach_out_email(name, email, phone_number, msg):
	#reach out template return in html form
	data = {
		"name" : name,
		"email":email,
		"phone_number":phone_number,
		"msg":msg,
	}
	subject = "Customer - {0} is trying to reach us"
	subject = subject.format(name)
	html_content = render_to_string('email/reach_out_email.html', data)
	msg = EmailMultiAlternatives(subject, "Email", 'info@lifeofchai.in', ['shubhamdabre@lifeofchai.in'])
	msg.attach_alternative(html_content, "text/html")
	msg.send()
	return "success"

def authenticate(username=None, password=None):
    try:
    	user = User.objects.get(username=username)
    	is_correct_pass = check_password(password,user.password)
    	if is_correct_pass:
    		return True
    	else:
    		return False
    except User.DoesNotExist:
        return False
    return False

def get_user(self, user_id):
    try:
        return User.objects.get(pk=user_id)
    except User.DoesNotExist:
        return None

def save_blog(request):
	title = request.POST.get('title')
	header_img_url = request.POST.get('header_img_url')
	meta_tags = request.POST.get('meta_tags')
	body = request.POST.get('blog_body')
	slug = slugify(title)
	username = request.session.get('username')
	print(title,header_img_url,meta_tags,body,username)
	blog = Blog.objects.create(title=title,meta_tags=meta_tags,body=body, 
		username=username, slug=slug,header_image_url=header_img_url)


def get_next_blogs(id):
	last_id = Blog.objects.filter(is_active=True).last().id
	if id >= last_id:
		#then return last 2 query sets
		return Blog.objects.filter(is_active=True).order_by('-id')[1:3]
	else:
		blog_list = []
		current_blog = Blog.objects.get(id=id)
		blog_list.append(current_blog.get_previous_by_created_at())
		blog_list.append(current_blog.get_next_by_created_at())
		return blog_list

def get_followers_count():
	path = apps.get_app_config('main').path
	complete_file_path = path + '/' + 'social_media_count.json'
	file = open(complete_file_path, 'r')
	data = json.loads(file.read())
	return data

def send_contact_us_email(request):
	'''
		send email that person has contacted you
	'''
	data = {
		"name" : request.POST.get('name',''),
		"email":request.POST.get('email',''),
		"phone":request.POST.get('phone',''),
		"msg":request.POST.get('msg',''),
	}
	if data['name'] and data['email'] and data['phone'] and data['msg']:
		subject = "Customer - {0} contacted Us."
		subject = subject.format(data['name'])
		html_content = render_to_string('email/contacted_us_email.html', data)
		msg = EmailMultiAlternatives(subject, "Email", 'info@lifeofchai.in', ['shubhamdabre@lifeofchai.in'])
		msg.attach_alternative(html_content, "text/html")
		msg.send()
		return "success"

def send_franchise_contacted_email(request):
	'''
		send email that person has contacted you
	'''
	data = {
		"name" : request.POST.get('name',''),
		"email":request.POST.get('email',''),
		"phone":request.POST.get('phone',''),
		"msg":request.POST.get('msg',''),
	}
	if data['name'] and data['email'] and data['phone'] and data['msg']:
		subject = "Customer - {0} contacted Us for Franchise"
		subject = subject.format(data['name'])
		html_content = render_to_string('email/contacted_us_email.html', data)
		msg = EmailMultiAlternatives(subject, "Email", 'info@lifeofchai.in', ['shubhamdabre@lifeofchai.in'])
		msg.attach_alternative(html_content, "text/html")
		msg.send()
		return "success"



