from django.http import HttpResponse
from django.shortcuts import render,redirect
from django.views.decorators.csrf import csrf_exempt
from main.utility import *
from main.models import Blog, NewsLetterSubscription
from main.utility import get_followers_count
import pdb;

def create_blog(request):
    '''
        create blog from here
    '''
    if request.session.get('username', ''):
        if request.method == 'GET':
            return render(request, 'create_blog.html')
        if request.method == 'POST':
            save_blog(request)
            return redirect('admin_home')
    else:
        return redirect('index')

def admin_login(request):
    if request.method == 'GET':
        return render(request, 'admin_login.html')
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        if authenticate(username,password):
            request.session['username'] = username 
            return redirect('admin_home')
        else:
            data = {
                'msg' : 'Invalid Crendentials!'
            }
            return render(request, 'admin_login.html', data)

def admin_home(request):
    if request.session.get('username',''):
        data = {
            'followers' : get_followers_count()
        }
        return render(request, 'admin_home.html', data)
    else:
        return redirect('index')

def admin_logout(request):
    request.session['username'] = ''
    return redirect('index')

def newsletter_data(request):
    if request.method == 'GET' and request.session.get('username',''):
        data = {
            'newsletter' : NewsLetterSubscription.objects.filter(is_active=True)[0:50]
        }
        return render(request,'newsletter_subscription.html',data)